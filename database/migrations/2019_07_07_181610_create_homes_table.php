<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('build_year');
            $table->float('house_square', 8, 2);
            $table->tinyInteger('kitchen_count');
            $table->smallInteger('pieses_count');
            //$table->tinyInteger('number_of_flors');
            $table->tinyInteger('flors_count');
            $table->float('living_square', 8, 2);
            $table->tinyInteger('count_parking_places');
            $table->tinyInteger('is_garden');
            $table->tinyInteger('rooms_count');
            $table->tinyInteger('bathrooms_count');
            $table->string('coordinates');
            $table->string('reference');
            $table->string('image_folder');

            $table->bigInteger('id_agent')->unsigned();
            $table->bigInteger('id_house_material')->unsigned();
            $table->bigInteger('id_gas_type')->unsigned();
            $table->bigInteger('id_water_type')->unsigned();
            $table->bigInteger('id_electricity_type')->unsigned();
            $table->bigInteger('id_sewage_type')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
