<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignHomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('homes', function (Blueprint $table) {
            $table->foreign('id_agent')->references('id')->on('agents');
            $table->foreign('id_house_material')->references('id')->on('house_materials');
            $table->foreign('id_gas_type')->references('id')->on('gas_types');
            $table->foreign('id_water_type')->references('id')->on('water_types');
            $table->foreign('id_electricity_type')->references('id')->on('electricity_types');
            $table->foreign('id_sewage_type')->references('id')->on('sewage_types');
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homes', function (Blueprint $table) {
            $table->dropForeign('homes_id_agent_foreign');
            $table->dropForeign('homes_id_house_material_foreign');
            $table->dropForeign('homes_id_gas_type_foreign');
            $table->dropForeign('homes_id_water_type_foreign');
            $table->dropForeign('homes_id_electricity_type_foreign');
            $table->dropForeign('homes_id_sewage_type_foreign');
            
        } );
    }
}
